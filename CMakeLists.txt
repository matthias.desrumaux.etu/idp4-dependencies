cmake_minimum_required(VERSION 3.1)

add_subdirectory( googletest )
add_subdirectory( jinja )
add_subdirectory( pistache )
add_subdirectory( json )
add_subdirectory( poco )
add_subdirectory( utils )