FROM ubuntu:18.04

RUN apt-get update -y \
    && apt-get upgrade -y \
    && apt-get install -y build-essential cmake

WORKDIR /idp

COPY . lib
RUN echo "cmake_minimum_required(VERSION 3.1)\nadd_subdirectory( lib )" > CMakeLists.txt
RUN mkdir builds && cd builds && cmake .. && make